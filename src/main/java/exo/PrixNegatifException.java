package exo;

public class PrixNegatifException extends Exception{
	
	private Double prixHT;

	public PrixNegatifException(Double prixHT) {
		super("prix negative: " + prixHT);
		this.prixHT = prixHT;
	}

	public Double getPrixHT() {
		return prixHT;
	}

	public void setPrixHT(Double prixHT) {
		this.prixHT = prixHT;
	}
	
	
}
