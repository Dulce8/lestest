package lestests;

import org.junit.Assert;
import org.junit.Test;

import exo.Article;
import exo.PrixNegatifException;


public class ArticleTest {

	@Test
	public void calculTTC_QuandLePrixHorsTaxesEst1000_AlorsLePrixTTCEst12206() throws Exception {
		
		Article article = new Article("dummy reference","dummy designation", 1000d);
		double prixTTC = article.calculTTC();
		Assert.assertEquals(1206, prixTTC, 0);
	}
	
	
	//test d'une exception, je teste qui plant
	@Test
	public void setPrixHT_QuandLePrixEstNegatif_AlorsPrixNegatifException() throws Exception {
		
		Article article = new Article("dummy reference","dummy designation");
		try {
			article.setPrixHT(-10d);
			//si je suis arrivé là c'est que mon code n'est pas arrive à l'exception 
			//et donc il faut la planté assert.fail
			Assert.fail("PrixNegatifException attendu");
		}catch (PrixNegatifException e) {
			//Assert, je teste que ça plante
			Assert.assertEquals(-10, e.getPrixHT(), 0);
		}
			
		
	}
}
