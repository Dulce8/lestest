package lestests;

import org.junit.Assert;
import org.junit.Test;

public class MaPremiereClasseDeTest {
	@Test
	public void monPremierTest() {
		
	}
	
	@Test
	public void abs_QuandNombrePositif_AlorsLeResultatEstLeNombre() {
		//arrange
			double leNombre = 1234;
			
		//act
			double resultat = Math.abs(leNombre);
		
		//assert
			Assert.assertEquals(leNombre, resultat, 0);
	}
	
	@Test
	public void abs_QuandNombreNegatif_AlorsLeResultatEstLeNombre() {
		//arrange
			double leNombre = -1234;
			
		//act
			double resultat = Math.abs(leNombre);
		
		//assert
			Assert.assertEquals(1234, resultat, 0);
	}
	
	@Test
	public void abs_QuandNombreNegatif_AlorsLeResultatEstCero() {
		//arrange
			double leNombre = 0;
			
		//act
			double resultat = Math.abs(leNombre);
		
		//assert
			Assert.assertEquals(0, resultat, 0);
	}
}
